(function($){

	$(document).ready(function(){
		
		// program for copyright text
		wp.customize('copyright_text', function(value){
			value.bind(function(to){
				$('h2').text(to);
			});
		});

		// program to change copyright color
		wp.customize('copyright_color', function(value){
			value.bind(function(to){
				$('h2').css('color', to);
			});
		});

		// program for checkbox whether copyright text show or not
		wp.customize('copyright_visibility', function(value){
			value.bind(function(to){
				false === to ? $('h2').hide() : $('h2').show();
			});
		});

		// program for selecting programming labguage
		wp.customize('language_selection', function(value){
			value.bind(function(to){
				$('.language').text('Favourite Language: ' + to);
			});
		});

	});


})(jQuery)