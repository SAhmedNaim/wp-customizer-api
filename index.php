<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<?php 
		// slider with redux framework
		global $basictheme;  

		$sliders = $basictheme['opt-home_sliders'];

		foreach ($sliders as $slider) 
		{
			echo $slider['title'];
			echo '<br/>';

			echo $slider['sub-title'];
			echo '<br/>';

			echo $slider['description'];
			echo '<br/>';

			echo $slider['url'];
			echo '<br/>';

			$image = $slider['image'];

			echo '<img src="'.$image.'" alt="Image Missing" width="300px" height="200px"/>';
			echo '<hr/>';
		}


		// echo $basictheme['opt-home_sliders'][0]['title'];
		// echo '<br/>';

		// echo $basictheme['opt-home_sliders'][0]['sub-title'];
		// echo '<br/>';

		// echo $basictheme['opt-home_sliders'][0]['description'];
		// echo '<br/>';

		// echo $basictheme['opt-home_sliders'][0]['url'];
		// echo '<br/>';

	?>

	<!-- Customizer API -->
	<img src="<?php echo get_theme_mod( 'logo_image' ); ?>" alt="Missing"/>

	<hr/>

	<?php if(true === get_theme_mod('copyright_visibility')) : ?>
		<h2><?php echo get_theme_mod( 'copyright_text' ); ?></h2>
	<?php endif; ?>

	<p class="language">Favourite Language: <?php echo get_theme_mod( 'language_selection' ); ?></p>

	<?php wp_footer(); ?>
</body>
</html>