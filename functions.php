<?php

function basictheme_init()
{
	add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'basictheme_init' );


// working with customizer API
function customizerAPI($api)
{
	// creating an options
	$api -> add_section('general_section', array(
		'title'		=> 'General Options',
		'priority'	=> 10
	));

	// making fields for copyright text
	$api -> add_setting('copyright_text', array(
		'default'	=> 'All rights reserved',
		'transport'	=> 'postMessage'
	));

	$api -> add_control('copyright_text', array(
		'section'	=> 'general_section',
		'label'		=> 'Copyright Text',
		'type'		=> 'text'
	));

	// making fields for logo upload
	$api -> add_setting('logo_image', array(
		'default' 	=> '',
		'transport'	=> 'refresh'
	));

	$api -> add_control(
		new WP_Customize_Image_Control( $api, 'logo_image', array(
			'label'		=> 'logo Uploader',
			'section'	=> 'general_section',
			'settings'	=> 'logo_image'
		))
	);

	// creating copyright color section
	$api -> add_section( 'colors_section', array(
		'title'		=> 'Colors Section',
		'priority'	=> 20
	) );

	$api -> add_setting( 'copyright_color', array(
		'default'	=> '#000000',
		'transport'	=> 'postMessage'
	) );

	$api -> add_control(
		new WP_Customize_Color_Control( $api, 'copyright_color', array(
			'section'	=> 'colors_section',
			'label'		=> 'Copyright Text Colors',
			'settings'	=> 'copyright_color'
		) )
	);

	// creating checkbox section whether copyright text will show or not
	$api -> add_section( 'visibility_option', array(
		'title'		=> 'Visibility',
		'priority'	=> 41
	) );

	$api -> add_setting( 'copyright_visibility', array(
		'default'	=> true,
		'transport'	=> 'postMessage'
	) );

	$api -> add_control( 'copyright_visibility', array(
		'section'	=> 'visibility_option',
		'label'		=> 'Show Copyright Text',
		'type'		=> 'checkbox'
	) );

	// creating options for selecting programming language
	$api -> add_setting( 'language_selection', array(
		'default'	=> 'Select Languages',
		'transport'	=> 'postMessage'
	) );

	$api -> add_control( 'language_selection', array(
		'section'	=> 'visibility_option',
		'label'		=> 'Choose Programming Languages',
		'type'		=> 'select',
		'choices'	=> array(
			'Select Languages'	=> 'Select Languages',
			'Java'		=> 'Java',
			'PHP'		=> 'PHP',
			'C++'		=> 'C++',
			'C#'		=> 'C#',
			'Python'	=> 'Python'
		)
	) );


}
add_action( 'customize_register', 'customizerAPI' );


// customizer script
function scripts_for_customizer()
{
	wp_enqueue_script( 'customizer-scripts', get_template_directory_uri().'/js/customizer_scripts.js', array('jquery', 'customize-preview') );
}
add_action( 'customize_preview_init', 'scripts_for_customizer' );

// customize scripts for web page
function inline_scripts()
{
?>
	<style type="text/css">
		
		h2 {
			color: <?php echo get_theme_mod( 'copyright_color' ); ?>;
		}

	</style>
<?php
}
add_action( 'wp_head', 'inline_scripts' );

// Require Redux Frameworks
// Redux Core
if(file_exists(dirname(__FILE__).'/lib/ReduxCore/framework.php'))
{
	require_once dirname(__FILE__).'/lib/ReduxCore/framework.php';
}

// This file is editable according to client's requirement
if(file_exists(dirname(__FILE__).'/lib/sample/example.php'))
{
	require_once dirname(__FILE__).'/lib/sample/example.php';
}
